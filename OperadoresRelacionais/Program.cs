﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperadoresRelacionais
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;

            Console.Write("Digite o valor de A:");
            a = int.Parse(Console.ReadLine());

            Console.Write("Digite o valor de B:");
            b = int.Parse(Console.ReadLine());

            if (b > a)
            {
                Console.Write("B (" + b + ") é maior que A (" + a + ")");
            }
            else if (a == b)
            {
                Console.Write("Ambos são iguais!");
            }
            else
            {
                Console.Write("A (" + a + ") é maior que B (" + b + ")");
            }

            Console.ReadKey();
        }
    }
}
